# Just a Dream

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/just-a-dream.git
cd just-a-dream
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```
