import com.matthewprenger.cursegradle.CurseProject
import com.matthewprenger.cursegradle.CurseRelation
import com.matthewprenger.cursegradle.Options
import net.fabricmc.loom.task.RemapJarTask

val minecraftVersion: String by project
val curseProjectId: String by project
val curseMinecraftVersion: String by project
val basePackage: String by project
val modJarBaseName: String by project
val modMavenGroup: String by project
val modId: String by project

plugins {
    java
    idea
    `maven-publish`
    id("fabric-loom") version "0.2.7-SNAPSHOT"
    id("com.matthewprenger.cursegradle") version "1.4.0"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

base {
    archivesBaseName = "$modJarBaseName-mc$minecraftVersion-fabric"
}

repositories {
    mavenCentral()
    jcenter()
    maven(url = "http://maven.fabricmc.net")
    maven(url = "https://maven.onyxstudios.dev")
}

version = "0.1.0"
group = modMavenGroup

minecraft {
    accessWidener = file("src/main/resources/$modId.accesswidener")
}

dependencies {
    minecraft("com.mojang:minecraft:$minecraftVersion")
    mappings("net.fabricmc:yarn:$minecraftVersion+build.14:v2")
    modImplementation("net.fabricmc:fabric-loader:0.8.2+build.194")

    modImplementation("net.fabricmc.fabric-api:fabric-api:0.5.1+build.294-1.15")

    modImplementation("com.github.NerdHubMC.Cardinal-Components-API:cardinal-components-base:2.3.0")
    include("com.github.NerdHubMC.Cardinal-Components-API:cardinal-components-base:2.3.0")

    modImplementation("com.github.NerdHubMC.Cardinal-Components-API:cardinal-components-entity:2.3.0")
    include("com.github.NerdHubMC.Cardinal-Components-API:cardinal-components-entity:2.3.0")

    modImplementation("me.shedaniel.cloth:config-2:2.13.5")
    include("me.shedaniel.cloth:config-2:2.13.5")

    modImplementation("me.sargunvohra.mcmods:autoconfig1u:2.0.1")
    include("me.sargunvohra.mcmods:autoconfig1u:2.0.1")

    modRuntime("io.github.prospector:modmenu:1.10.2+build.32")
    modCompileOnly("io.github.prospector:modmenu:1.10.2+build.32")
}

val processResources = tasks.getByName<ProcessResources>("processResources") {
    inputs.property("version", project.version)

    filesMatching("fabric.mod.json") {
        filter { line -> line.replace("%VERSION%", "${project.version}") }
    }
}

val javaCompile = tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

val jar = tasks.getByName<Jar>("jar") {
    from("LICENSE")
}

val remapJar = tasks.getByName<RemapJarTask>("remapJar")

    curseforge {
        if (project.hasProperty("curseforge_api_key")) {
            apiKey = project.property("curseforge_api_key")!!
        }

        project(closureOf<CurseProject> {
            id = curseProjectId
            releaseType = "release"
            addGameVersion(curseMinecraftVersion)
            addGameVersion("Fabric")
            relations(closureOf<CurseRelation> {
                requiredDependency("fabric-api")
                embeddedLibrary("cardinal-components")
            })
            mainArtifact(file("${project.buildDir}/libs/${base.archivesBaseName}-$version.jar"))
            afterEvaluate {
                mainArtifact(remapJar)
                uploadTask.dependsOn(remapJar)
            }
        })

        options(closureOf<Options> {
            forgeGradleIntegration = false
        })
    }
