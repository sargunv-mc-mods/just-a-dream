package me.sargunvohra.mcmods.justadream;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.ConfigHolder;
import me.sargunvohra.mcmods.autoconfig1u.serializer.Toml4jConfigSerializer;
import me.sargunvohra.mcmods.justadream.command.JustADreamCommand;
import me.sargunvohra.mcmods.justadream.config.JustADreamConfig;
import me.sargunvohra.mcmods.justadream.config.JustADreamGameRules;
import me.sargunvohra.mcmods.justadream.dream.DreamLoader;
import me.sargunvohra.mcmods.justadream.roll.DreamRoller;
import me.sargunvohra.mcmods.justadream.roll.LevelDreamRoller;
import nerdhub.cardinal.components.api.ComponentRegistry;
import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.event.EntityComponentCallback;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.CommandRegistry;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.resource.ResourceType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class JustADreamInit implements ModInitializer {

    public static final ConfigHolder<JustADreamConfig> CONFIG_HOLDER =
        AutoConfig.register(JustADreamConfig.class, Toml4jConfigSerializer::new);

    public static final DreamLoader DREAM_LOADER = new DreamLoader();
    public static final StatusEffect HAUNT = new NonClearableStatusEffect(StatusEffectType.HARMFUL, 4411786);
    private static final Identifier DREAM_ROLLER_ID = new Identifier("justadream", "dream_roller");
    public static final ComponentType<DreamRoller> DREAM_ROLLER =
        ComponentRegistry.INSTANCE.registerIfAbsent(DREAM_ROLLER_ID, DreamRoller.class)
            .attach(EntityComponentCallback.event(ServerPlayerEntity.class), LevelDreamRoller::new);
    private static final Identifier HAUNT_ID = new Identifier("justadream", "haunt");

    @Override
    public void onInitialize() {
        Registry.register(Registry.STATUS_EFFECT, HAUNT_ID, HAUNT);
        CommandRegistry.INSTANCE.register(false, dispatcher -> dispatcher.register(JustADreamCommand.buildRootCommand()));
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(DREAM_LOADER);
        JustADreamGameRules.init();
    }
}
