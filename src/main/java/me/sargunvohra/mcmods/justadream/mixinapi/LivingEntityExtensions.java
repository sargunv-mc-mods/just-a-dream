package me.sargunvohra.mcmods.justadream.mixinapi;

import net.minecraft.entity.effect.StatusEffectInstance;

import java.util.List;

public interface LivingEntityExtensions {
    List<StatusEffectInstance> getEffectsToRestore();
}
