package me.sargunvohra.mcmods.justadream.config;

import net.minecraft.world.GameRules;

public class JustADreamGameRules {
    public static final GameRules.RuleKey<GameRules.BooleanRule> ENABLED =
        GameRules.register("justadream:doRollOnWake", GameRules.BooleanRule.create(true));

    public static final GameRules.RuleKey<GameRules.IntRule> ROLL_PENALTY_COOLDOWN_DAYS =
        GameRules.register("justadream:rollPenaltyCooldownDays", GameRules.IntRule.create(8));

    public static final GameRules.RuleKey<GameRules.IntRule> ROLL_EXTRA_PENALTY =
        GameRules.register("justadream:rollExtraPenalty", GameRules.IntRule.create(0));

    public static final GameRules.RuleKey<GameRules.IntRule> NUM_DICE =
        GameRules.register("justadream:numDice", GameRules.IntRule.create(4));

    public static final GameRules.RuleKey<GameRules.IntRule> NUM_DICE_FACES =
        GameRules.register("justadream:numFacesPerDie", GameRules.IntRule.create(3));

    public static final GameRules.RuleKey<GameRules.IntRule> DICE_MIN_VALUE =
        GameRules.register("justadream:minValuePerDie", GameRules.IntRule.create(0));

    public static void init() {
    }
}
