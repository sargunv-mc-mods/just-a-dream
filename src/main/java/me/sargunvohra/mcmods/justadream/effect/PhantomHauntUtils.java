package me.sargunvohra.mcmods.justadream.effect;

import me.sargunvohra.mcmods.justadream.JustADreamInit;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.TranslatableText;

public class PhantomHauntUtils {
    public static void startHaunt(ServerPlayerEntity player) {
        StatusEffectInstance effect = new StatusEffectInstance(
            JustADreamInit.HAUNT,
            24000,
            0,
            false,
            true
        );
        player.addStatusEffect(effect);
        player.addChatMessage(new TranslatableText("text.justadream.haunt.start"), false);
    }

    public static void endHaunt(ServerPlayerEntity player) {
        StatusEffectInstance effect = player.getStatusEffect(JustADreamInit.HAUNT);
        if (effect != null) {
            effect.duration = 0;
        }
    }
}
