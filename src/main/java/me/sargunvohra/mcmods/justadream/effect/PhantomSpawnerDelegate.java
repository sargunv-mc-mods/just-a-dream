package me.sargunvohra.mcmods.justadream.effect;

import me.sargunvohra.mcmods.justadream.JustADreamInit;
import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnType;
import net.minecraft.entity.mob.PhantomEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.SpawnHelper;

import java.util.Objects;
import java.util.Random;

public class PhantomSpawnerDelegate {
    private int ticksUntilNextSpawn;

    private static boolean isEligible(ServerPlayerEntity player) {
        if (player.isSpectator())
            return false;
        return player.hasStatusEffect(JustADreamInit.HAUNT);
    }

    private static boolean isExposed(ServerPlayerEntity player) {
        ServerWorld world = player.getServerWorld();
        BlockPos pos = player.getBlockPos();
        if (!world.dimension.hasSkyLight())
            return true;
        return pos.getY() > world.getSeaLevel() && world.isSkyVisible(pos);
    }

    private static int spawnPack(ServerWorld world, BlockPos pos) {
        int packSize = world.random.nextInt(world.getDifficulty().getId() + 1);
        EntityData entityData = null;
        for (int i = 0; i < packSize; ++i) {
            PhantomEntity phantomEntity = Objects.requireNonNull(EntityType.PHANTOM.create(world));
            phantomEntity.refreshPositionAndAngles(pos, 0.0F, 0.0F);
            entityData = phantomEntity.initialize(world, world.getLocalDifficulty(pos), SpawnType.NATURAL, entityData, null);
            world.spawnEntity(phantomEntity);
        }
        return packSize;
    }

    public int spawn(ServerWorld world, boolean spawnMonsters, boolean spawnAnimals) {
        if (!spawnMonsters || --this.ticksUntilNextSpawn > 0)
            return 0;

        Random random = world.random;
        this.ticksUntilNextSpawn += (45 + random.nextInt(45)) * 20;

        return world.getPlayers().stream()
            .filter(PhantomSpawnerDelegate::isEligible)
            .filter(PhantomSpawnerDelegate::isExposed)
            .map(player -> player.getBlockPos().up(15 + random.nextInt(30)).east(-10 + random.nextInt(21)).south(-10 + random.nextInt(21)))
            .filter(pos -> SpawnHelper.isClearForSpawn(world, pos, world.getBlockState(pos), world.getFluidState(pos)))
            .mapToInt(pos -> spawnPack(world, pos))
            .sum();
    }
}
