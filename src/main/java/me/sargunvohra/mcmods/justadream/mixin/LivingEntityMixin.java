package me.sargunvohra.mcmods.justadream.mixin;

import me.sargunvohra.mcmods.justadream.mixinapi.LivingEntityExtensions;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.mob.FlyingEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.List;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity implements LivingEntityExtensions {
    private final List<StatusEffectInstance> effectsToRestore = new ArrayList<>();

    private LivingEntityMixin(EntityType<? extends FlyingEntity> type, World world) {
        super(type, world);
    }

    @Override
    public List<StatusEffectInstance> getEffectsToRestore() {
        return this.effectsToRestore;
    }

    @Shadow
    public abstract boolean addStatusEffect(StatusEffectInstance effect);

    @Inject(method = "clearStatusEffects", at = @At("RETURN"))
    public void reapplySavedEffects(CallbackInfoReturnable<Boolean> ci) {
        if (!world.isClient) {
            List<StatusEffectInstance> savedHaunts = this.getEffectsToRestore();
            savedHaunts.forEach(this::addStatusEffect);
            savedHaunts.clear();
        }
    }
}
