package me.sargunvohra.mcmods.justadream.mixin;

import me.sargunvohra.mcmods.justadream.effect.PhantomSpawnerDelegate;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.gen.PhantomSpawner;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PhantomSpawner.class)
public abstract class PhantomSpawnerMixin {
    private final PhantomSpawnerDelegate justadream_ = new PhantomSpawnerDelegate();

    /**
     * @author sargunv
     * @reason we're totally replacing vanilla phantom spawn mechanics
     */
    @Overwrite
    public int spawn(
        ServerWorld serverWorld,
        boolean spawnMonsters,
        boolean spawnAnimals
    ) {
        return justadream_.spawn(serverWorld, spawnMonsters, spawnAnimals);
    }
}
