package me.sargunvohra.mcmods.justadream.mixin;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.FlyingEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.mob.PhantomEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

@Mixin(PhantomEntity.class)
public abstract class PhantomEntityMixin extends FlyingEntity implements Monster {
    private PhantomEntityMixin(EntityType<? extends FlyingEntity> type, World world) {
        super(type, world);
        throw new IllegalStateException();
    }

    /**
     * Make phantoms no longer burn in daylight
     */
    @ModifyArg(
        method = "tickMovement",
        at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/mob/PhantomEntity;setOnFireFor(I)V"),
        index = 0
    )
    public int adjustFireSeconds(int original) {
        return 0;
    }
}
