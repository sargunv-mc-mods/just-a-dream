package me.sargunvohra.mcmods.justadream;

import me.sargunvohra.mcmods.justadream.mixinapi.LivingEntityExtensions;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AbstractEntityAttributeContainer;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffectType;

public class NonClearableStatusEffect extends StatusEffect {

    public NonClearableStatusEffect(StatusEffectType type, int color) {
        super(type, color);
    }

    @Override
    public void onRemoved(LivingEntity entity, AbstractEntityAttributeContainer attributes, int amplifier) {
        StatusEffectInstance effect = entity.getStatusEffect(this);
        if (effect != null && effect.getDuration() > 0) {
            // lol nice try but nope
            ((LivingEntityExtensions) entity).getEffectsToRestore().add(effect);
        }
        super.onRemoved(entity, attributes, amplifier);
    }
}
