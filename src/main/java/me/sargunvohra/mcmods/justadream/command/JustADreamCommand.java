package me.sargunvohra.mcmods.justadream.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import me.sargunvohra.mcmods.justadream.dream.Dream;
import me.sargunvohra.mcmods.justadream.effect.PhantomHauntUtils;
import me.sargunvohra.mcmods.justadream.roll.DreamRoller;
import me.sargunvohra.mcmods.justadream.roll.DreamRollerUtils;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.TranslatableText;

import java.util.Optional;

import static net.minecraft.command.arguments.EntityArgumentType.getPlayer;
import static net.minecraft.command.arguments.EntityArgumentType.player;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class JustADreamCommand {

    private static LiteralArgumentBuilder<ServerCommandSource> buildRollCommand() {
        return literal("roll")
            .then(
                argument("player", player())
                    .executes(context -> {
                        ServerPlayerEntity player = getPlayer(context, "player");
                        Optional<Dream> maybeDream = DreamRollerUtils.roll(player);
                        maybeDream.ifPresent(dream -> {
                            dream.apply(player);
                            context.getSource().sendFeedback(
                                new TranslatableText(
                                    "commands.justadream.roll.apply", dream.id, player.getDisplayName()
                                ),
                                true
                            );
                        });
                        return 0;
                    })
            );
    }

    private static LiteralArgumentBuilder<ServerCommandSource> buildHauntCommand() {
        return literal("haunt")
            .then(
                literal("start")
                    .then(
                        argument("player", player())
                            .executes(context -> {
                                ServerPlayerEntity player = getPlayer(context, "player");
                                PhantomHauntUtils.startHaunt(player);
                                context.getSource().sendFeedback(
                                    new TranslatableText("commands.justadream.haunt.start", player.getDisplayName()), true
                                );
                                return 0;
                            })
                    )
            )
            .then(
                literal("end")
                    .then(
                        argument("player", player())
                            .executes(context -> {
                                ServerPlayerEntity player = getPlayer(context, "player");
                                PhantomHauntUtils.endHaunt(player);
                                context.getSource().sendFeedback(
                                    new TranslatableText("commands.justadream.haunt.end", player.getDisplayName()), true
                                );
                                return 0;
                            })
                    )
            );
    }

    private static LiteralArgumentBuilder<ServerCommandSource> buildDifficultyCommand() {
        return literal("penalty")
            .then(
                literal("check")
                    .then(
                        argument("player", player())
                            .executes(context -> {
                                ServerPlayerEntity player = getPlayer(context, "player");
                                context.getSource().sendFeedback(
                                    new TranslatableText(
                                        "commands.justadream.penalty.check",
                                        player.getDisplayName(),
                                        DreamRollerUtils.getRoller(player).getPenalty()
                                    ),
                                    false
                                );
                                return 0;
                            })
                    )
            )
            .then(
                literal("clear")
                    .then(
                        argument("player", player())
                            .executes(context -> {
                                ServerPlayerEntity player = getPlayer(context, "player");
                                DreamRoller roller = DreamRollerUtils.getRoller(player);
                                roller.clearPenalty();
                                context.getSource().sendFeedback(
                                    new TranslatableText(
                                        "commands.justadream.penalty.clear",
                                        player.getDisplayName(),
                                        roller.getPenalty()
                                    ),
                                    true
                                );
                                return 0;
                            })
                    )
            );
    }

    public static LiteralArgumentBuilder<ServerCommandSource> buildRootCommand() {
        return literal("justadream")
            .requires(source -> source.hasPermissionLevel(2))
            .then(buildRollCommand())
            .then(buildDifficultyCommand())
            .then(buildHauntCommand());
    }
}
