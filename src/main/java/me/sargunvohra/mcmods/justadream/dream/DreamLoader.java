package me.sargunvohra.mcmods.justadream.dream;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.function.CommandFunction;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DreamLoader extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = new Gson();

    private final List<Dream> dreams = new ArrayList<>();

    public DreamLoader() {
        super(GSON, "justadream");
    }

    @Override
    protected void apply(Map<Identifier, JsonObject> loader, ResourceManager manager, Profiler profiler) {
        loader.forEach((id, json) -> {
            Dream dream = GSON.fromJson(json, JsonDream.class).getDream(id);
            LOGGER.info(dream.toString());
            dreams.add(dream);
        });
        LOGGER.info("Loaded {} dreams", dreams.size());
    }

    public Optional<Dream> getFirstMatching(int value) {
        return dreams.stream()
            .filter(dream -> value >= dream.min && value <= dream.max)
            .findFirst();
    }

    @Override
    public Identifier getFabricId() {
        return new Identifier("justadream", "dream_loader");
    }

    private static class JsonDream {
        int min = Integer.MIN_VALUE;
        int max = Integer.MAX_VALUE;
        String function = null;

        Dream getDream(Identifier id) {
            CommandFunction.LazyContainer functionContainer = function != null
                ? new CommandFunction.LazyContainer(new Identifier(function))
                : CommandFunction.LazyContainer.EMPTY;
            return new Dream(id, min, max, functionContainer);
        }
    }

}
