package me.sargunvohra.mcmods.justadream.dream;

import net.minecraft.server.function.CommandFunction;
import net.minecraft.server.function.CommandFunctionManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public class Dream {
    public final Identifier id;
    public final int min;
    public final int max;
    public final CommandFunction.LazyContainer function;

    public Dream(
        Identifier id,
        int min,
        int max,
        CommandFunction.LazyContainer function
    ) {
        this.id = id;
        this.min = min;
        this.max = max;
        this.function = function;
    }

    public void apply(ServerPlayerEntity player) {
        CommandFunctionManager functionManager = player.server.getCommandFunctionManager();
        function.get(functionManager).ifPresent((commandFunction) -> {
            functionManager.execute(commandFunction, player.getCommandSource().withSilent().withLevel(2));
        });
    }

    @Override
    public String toString() {
        return "Dream{" +
            "id=" + id +
            ", min=" + min +
            ", max=" + max +
            ", function=" + function.getId() +
            '}';
    }
}
