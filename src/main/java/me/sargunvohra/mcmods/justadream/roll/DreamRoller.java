package me.sargunvohra.mcmods.justadream.roll;

import nerdhub.cardinal.components.api.component.Component;

public interface DreamRoller extends Component {
    int rollForHaunt();

    void clearPenalty();

    int getPenalty();
}

