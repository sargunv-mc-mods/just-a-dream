package me.sargunvohra.mcmods.justadream.roll;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.GameRules;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;

import static me.sargunvohra.mcmods.justadream.config.JustADreamGameRules.*;

public class LevelDreamRoller implements DreamRoller {

    private static final Logger LOGGER = LogManager.getLogger();

    private final LinkedList<Long> rollTimestamps = new LinkedList<>();
    private final Random random = new Random();
    private final ServerPlayerEntity player;

    public LevelDreamRoller(ServerPlayerEntity player) {
        this.player = player;
    }

    private GameRules getGamerules() {
        return Objects.requireNonNull(player.getServer()).getGameRules();
    }

    private void cleanupHistory() {
        long cutoff = player.getServerWorld().getTime() - getGamerules().getInt(ROLL_PENALTY_COOLDOWN_DAYS) * 24000;
        while (!rollTimestamps.isEmpty() && rollTimestamps.peek() < cutoff)
            rollTimestamps.removeFirst();
    }

    @Override
    public int rollForHaunt() {
        GameRules gamerules = getGamerules();

        long now = player.getServerWorld().getTime();
        LOGGER.info("dream roll, current time is " + now);

        this.cleanupHistory();
        LOGGER.info("previous roll times: " + Arrays.toString(rollTimestamps.toArray()));

        int penalty = getPenalty();
        int roll = random.ints(
            gamerules.getInt(NUM_DICE),
            gamerules.getInt(DICE_MIN_VALUE),
            gamerules.getInt(DICE_MIN_VALUE) + gamerules.getInt(NUM_DICE_FACES)
        ).sum();
        int result = roll - penalty;
        LOGGER.info(String.format("(dice:%d) - (penalty:%d) = %d", roll, penalty, result));

        rollTimestamps.add(now);
        return result;
    }

    @Override
    public void clearPenalty() {
        rollTimestamps.clear();
    }

    @Override
    public int getPenalty() {
        return rollTimestamps.size() + getGamerules().getInt(ROLL_EXTRA_PENALTY);
    }

    @Override
    public void fromTag(CompoundTag compoundTag) {
        rollTimestamps.clear();
        for (long t : compoundTag.getLongArray("passTimestamps")) {
            rollTimestamps.add(t);
        }
    }

    @Override
    public CompoundTag toTag(CompoundTag compoundTag) {
        compoundTag.putLongArray("passTimestamps", rollTimestamps);
        return compoundTag;
    }
}
