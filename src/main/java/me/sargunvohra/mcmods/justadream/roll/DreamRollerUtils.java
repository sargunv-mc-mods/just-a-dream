package me.sargunvohra.mcmods.justadream.roll;

import me.sargunvohra.mcmods.autoconfig1u.shadowed.blue.endless.jankson.annotation.Nullable;
import me.sargunvohra.mcmods.justadream.JustADreamInit;
import me.sargunvohra.mcmods.justadream.config.JustADreamGameRules;
import me.sargunvohra.mcmods.justadream.dream.Dream;
import net.minecraft.server.network.ServerPlayerEntity;

import java.util.Optional;

public class DreamRollerUtils {
    @Nullable
    public static Optional<Dream> roll(ServerPlayerEntity player) {
        if (player.server.getGameRules().getBoolean(JustADreamGameRules.ENABLED))
            return JustADreamInit.DREAM_LOADER.getFirstMatching(getRoller(player).rollForHaunt());
        else return Optional.empty();
    }

    public static DreamRoller getRoller(ServerPlayerEntity player) {
        return JustADreamInit.DREAM_ROLLER.get(player);
    }
}
